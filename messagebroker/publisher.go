package messagebroker

import (
	"context"
	"errors"
)

var ErrTest = errors.New("error creating test message broker")

// MessageHandler is a function type for handling messages received from the message broker
type MessageHandler func([]byte) error

// MessageBroker defines the common methods for interacting with a message broker
type MessageBroker interface {
	// Publish sends a message to a specified topic
	Publish(context context.Context, topic string, message []byte) error

	// Subscribe subscribes to messages from a specified topic and calls the provided handler for each received message
	Subscribe(context context.Context, topic string, handler MessageHandler) error

	// Close closes the connection to the message broker
	Close(context context.Context) error
}

// PubSubBroker implements the MessageBroker interface for Google Cloud Pub/Sub
type PubSubBroker struct {
	// Add necessary Pub/Sub client fields here
}

func (p *PubSubBroker) Publish(topic string, message []byte) error {
	// Implement Pub/Sub message publishing logic here
	return nil
}

func (p *PubSubBroker) Subscribe(topic string, handler MessageHandler) error {
	// Implement Pub/Sub message subscription logic here
	return nil
}

func (p *PubSubBroker) Close() error {
	// Implement Pub/Sub connection closing logic here
	return nil
}
