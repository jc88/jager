module gitlab.com/jc88/jager/messagebroker

go 1.22.0

require github.com/confluentinc/confluent-kafka-go v1.9.2
