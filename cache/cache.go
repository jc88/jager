package cache

type Cache interface {
	// Set the cache key for the given cache key.
	Set(key string, value []byte) error
	Get(key string) ([]byte, error)
	Delete(key string) error
}
