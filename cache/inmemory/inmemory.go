package inmemory

import (
	"context"
	"fmt"
	"time"

	"github.com/allegro/bigcache/v3"
)

type memoryCache struct {
	client *bigcache.BigCache
}

func New(ctx context.Context) (*memoryCache, error) {
	config := bigcache.DefaultConfig(10 * time.Minute)

	bCache, err := bigcache.New(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("failed to create memory cache: %w", err)
	}

	return &memoryCache{
		client: bCache,
	}, nil
}

func (m *memoryCache) Set(key string, value []byte) error {
	return m.client.Set(key, value)
}

func (m *memoryCache) Get(key string) ([]byte, error) {
	return m.client.Get(key)
}

func (m *memoryCache) Delete(key string) error {
	return m.client.Delete(key)
}
